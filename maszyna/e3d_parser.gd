extends Node
class_name E3DParser

const SUBMODEL_GL_TRIANGLES = 4
const SUBMODEL_BANAN = 256

var banany = 0


func find_node_of_type(node: Node, type):
    if node is type:
        return node
    for x in node.get_children():
        if x is type:
            return x
        else:
            var sub = find_node_of_type(x, type)
            if sub:
                return sub
    push_error("Node " + str(node) + " has no "+String(type))
    return null

func find_meshinstance(node: Node) -> MeshInstance:
    return find_node_of_type(node, MeshInstance)

func find_collisionshape(node: Node) -> CollisionShape:
    return find_node_of_type(node, CollisionShape)

func set_owner_recursive(node, new_owner):
    node.owner = new_owner
    if node.get_child_count():
        for kid in node.get_children():
            set_owner_recursive(kid, new_owner)

const MAX_31B = 1 << 31
const MAX_32B = 1 << 32

func u32s(unsigned):
    return (unsigned + MAX_31B) % MAX_32B - MAX_31B

func _read_chunk_header(file):
    var chunk_id = file.get_buffer(4).get_string_from_ascii()
    var chunk_len = file.get_32()
    #if chunk_len % 4 > 0:
    #    chunk_len = int(chunk_len / 4) * 4 + 4

    return {"id": chunk_id, "len": chunk_len, "data_len": chunk_len-8}

func unsigned32_to_signed(unsigned):
    return (unsigned + MAX_31B) % MAX_32B - MAX_31B


func _read_submodel(file) -> Dictionary:
    var result = {
            "next_idx": unsigned32_to_signed(file.get_32()),
            "first_child_idx": unsigned32_to_signed(file.get_32()),
            "type": file.get_32(),
            "name_idx": u32s(file.get_32()),
            "anim": unsigned32_to_signed(file.get_32()),
            "flags": file.get_32() & 0xFFFF,
            "matrix_idx": unsigned32_to_signed(file.get_32()),
            "vertex_count": u32s(file.get_32()),
            "first_vertex_idx": u32s(file.get_32()),
            "material_idx": unsigned32_to_signed(file.get_32()),
            "visibility_threshold": file.get_float(),
            "light_threshold": file.get_float(),
            "vertices": PoolVector3Array(),
            "normals": PoolVector3Array(),
            "uv": PoolVector2Array(),
            "indices": PoolIntArray(),
        }
    file.get_buffer(68)  # skip to 116
    result["lod_max_distance"] = file.get_float()
    result["lod_min_distance"] = file.get_float()
    file.get_buffer(32) # skip attrs for light
    result["index_count"] = file.get_32()
    result["first_index_idx"] = file.get_32()
    result["transparent"] = result["flags"] & 0b000001
    file.get_buffer(92)

    return result


func _read_string0(file: File):
    var output = []
    while true:
        var c = file.get_8()
        if c == 0:
            break
        output.append(char(c))
    return "".join(output)


func _buffer_to_strings(buf) -> Array:
    var output = []
    var tmp = []
    for i in range(0, buf.size(), 1):
        var c = buf[i]
        if c == 0:
            output.append("".join(tmp))
            tmp = []
        else:
            tmp.append(char(c))
    return output

func _read_matrix(file: File):
    var o = []
    for i in range(0, 16):
        o.append(file.get_float())

    return Transform(
        Basis(
            Vector3(o[0], o[1], o[2]),
            Vector3(o[4], o[5], o[6]),
            Vector3(o[8], o[9], o[10])
        ),
        Vector3(o[12], o[13], o[14])
    )


func parse_e3d(file: File, options:Dictionary):
    var e3d0 = _read_chunk_header(file)
    var submodels = []
    var submodel_names = []
    var material_names = []
    var submodel_vertices = []
    var submodel_normals = []
    var submodel_uvs = []
    var submodel_indices = []
    var submodel_tangents = []
    var matrices = []

    while not file.eof_reached():
        var chunk = _read_chunk_header(file)
        #print("Reading chunk: ", chunk.id)
        match chunk.id:
            "SUB0":
                var submodels_count = chunk.data_len / 256
                for i in range(0, submodels_count, 1):
                    var sm = _read_submodel(file)
                    submodels.append(sm)
            "NAM0":
                submodel_names = _buffer_to_strings(file.get_buffer(chunk.data_len))
            "TEX0":
                material_names = _buffer_to_strings(file.get_buffer(chunk.data_len))
            "TRA0":
                var matrix_count = chunk.data_len / 64
                for i in range(0, matrix_count, 1):
                    matrices.append(_read_matrix(file))
            "IDX1":
                var _pos = file.get_position()
                for submodel in submodels:
                    file.seek(_pos + submodel["first_index_idx"])
                    var indices = PoolIntArray()
                    var ccw_index = PoolIntArray()
                    for i in range(0, submodel["index_count"]):
                        indices.append(file.get_8())
                    for i in range(0, indices.size(), 3):
                        ccw_index.append_array([indices[i], indices[i+2], indices[i+1]])
                    #indices.invert()
                    submodel_indices.append(ccw_index)
                file.seek(chunk.data_len + _pos)
            "IDX2":
                var _pos = file.get_position()
                for submodel in submodels:
                    file.seek(_pos + submodel["first_index_idx"] * 2)
                    var indices = PoolIntArray()
                    var ccw_index = PoolIntArray()
                    for i in range(0, submodel["index_count"]):
                        indices.append(file.get_16())
                    for i in range(0, indices.size(), 3):
                        ccw_index.append_array([indices[i], indices[i+2], indices[i+1]])
                    #indices.invert()
                    submodel_indices.append(ccw_index)
                file.seek(chunk.data_len + _pos)

            "VNT2":
                # 8 x 2 bytes
                var _pos = file.get_position()
                for submodel in submodels:
                    file.seek(_pos + submodel["first_vertex_idx"] * 48)

                    var vertices = PoolVector3Array()
                    var normals = PoolVector3Array()
                    var uvs = PoolVector2Array()
                    var tangents = PoolRealArray()

                    for i in range(0, submodel["vertex_count"]):
                        var x = file.get_float()
                        var y = file.get_float()
                        var z = file.get_float()
                        var nx = file.get_float()
                        var nz = file.get_float()
                        var ny = file.get_float()
                        var u = file.get_float()
                        var v = file.get_float()
                        var tx = file.get_float()
                        var ty = file.get_float()
                        var tz = file.get_float()
                        var tw = file.get_float()
                        var _vec = Vector3(x, y, z)
                        var _norm = Vector3(nx, ny, nz)
                        var _uv = Vector2(u, v)


                        vertices.append(_vec)
                        normals.append(_norm)
                        uvs.append(_uv)
                        tangents.append_array([tx, ty, tz, tw])

                    #vertices.invert()
                    #normals.invert()
                    #uvs.invert()
                    #tangents.invert()

                    submodel_vertices.append(vertices)
                    submodel_normals.append(normals)
                    submodel_uvs.append(uvs)
                    submodel_tangents.append(tangents)

                file.seek(chunk.data_len + _pos)
            _:
                # skip unsupported chunk
                push_warning("Skipping unsupported chunk: " + chunk.id)
                file.get_buffer(chunk.data_len)

    var i = 0

    for submodel in submodels:
        var name_idx = submodel["name_idx"]
        var tex_idx = submodel["material_idx"]
        var mtx_idx = submodel["matrix_idx"]
        submodel["indices"] = submodel_indices[i]
        submodel["vertices"] = submodel_vertices[i]
        submodel["normals"] = submodel_normals[i]
        submodel["tangents"] = submodel_tangents[i]
        submodel["uvs"] = submodel_uvs[i]
        submodel["name"] = submodel_names[name_idx] if name_idx > -1 else null
        submodel["material"] = material_names[tex_idx] if tex_idx > -1 else null
        submodel["matrix"] = matrices[mtx_idx] if mtx_idx > -1 else null
        i += 1
    return submodels


func _extend_aabb_recursive(node: Node, aabb: AABB):
    var child_aabb = aabb

    if node.get_child_count():
        for x in node.get_children():
            var x_aabb = _extend_aabb_recursive(x, child_aabb)
            child_aabb = child_aabb.merge(x_aabb)
        #child_aabb = node.transform.xform(child_aabb)
        #var _debugcs = CollisionShape.new()
        #_debugcs.shape = BoxShape.new()
        #_debugcs.shape.extents = child_aabb.size/2
        #node.add_child(_debugcs)
        #_debugcs.set_owner(node.owner)
        
    if node is MeshInstance:
        var node_aabb = node.transform.xform(node.get_aabb())
        
        #var _debugcs = CollisionShape.new()
        #_debugcs.transform = node.transform
        #_debugcs.shape = BoxShape.new()
        #_debugcs.shape.extents = node_aabb.size/2
        #node.get_parent().add_child(_debugcs)
        #_debugcs.set_owner(node.owner)        
        #print(node, " aabb: ", node_aabb)
        child_aabb = child_aabb.merge(node_aabb)
    else:
        child_aabb = node.transform.xform(child_aabb)
        
    return child_aabb
    

func _create_submodel(root, parent, submodel):
    match submodel["type"]:
        SUBMODEL_BANAN:
            var banan = Spatial.new()
            banan.name = "_banan"+str(banany)
            root.add_child(banan)
            banany += 1
            banan.owner = root
            banan.transform = submodel["matrix"]
            return banan

        SUBMODEL_GL_TRIANGLES:
            var _vc = submodel["vertices"].size()
            var _nc = submodel["normals"].size()
            var _uc = submodel["uvs"].size()
            var _ic = submodel["indices"].size()
            var _tc = submodel["tangents"].size() / 4

            # print(
            #    "Creating MeshInstance (name=%s, material=%s, anim=%s, transp=%s, flags=%s, vertices=%s, indices=%s, normals=%s, uvs=%s, tangents=%s)" % [
            #        submodel["name"], submodel["material_idx"], submodel["anim"], submodel["transparent"], submodel["flags"], _vc, _ic, _nc, _uc, _tc
            #    ])

            var obj = MaSzyna_SubModel.new()

            obj.animation = submodel["anim"]
            if submodel["material_idx"] < 0:
                obj.dynamic_material = true
                obj.dynamic_material_index = abs(submodel["material_idx"])-1
            obj.material_name = submodel["material"] if submodel["material"] else ""
            obj.lod_min_distance = sqrt(submodel["lod_min_distance"])
            obj.lod_max_distance = sqrt(submodel["lod_max_distance"])

            if obj.lod_min_distance > 0:
                obj.visible = false  # temp. disable LOD

            var mesh = ArrayMesh.new()
            var triangles = []

            triangles.resize(ArrayMesh.ARRAY_MAX)
            triangles[ArrayMesh.ARRAY_VERTEX] = submodel["vertices"]
            triangles[ArrayMesh.ARRAY_NORMAL] = submodel["normals"]
            triangles[ArrayMesh.ARRAY_TANGENT] = submodel["tangents"]
            triangles[ArrayMesh.ARRAY_TEX_UV] = submodel["uvs"]
            triangles[ArrayMesh.ARRAY_INDEX] = submodel["indices"]

            mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, triangles)
            mesh.regen_normalmaps()

            obj.mesh = mesh
            obj.transform = submodel["matrix"]
            if submodel.get("name"):
                obj.name = submodel["name"]
            #print("Adding object ", obj, " to node ", parent)
            parent.add_child(obj)
            set_owner_recursive(obj, root)
            return obj

func _add_submodels(root, parent, submodel, submodels):

    var created = _create_submodel(root, parent, submodel)
    var created_submodel = submodel
    #print("submodel %s [next=%s, child=%s] created=%s" % [submodel["name"], submodel["next_idx"], submodel["first_child_idx"], created])
    if submodel["next_idx"] > -1:
        submodel = submodels[submodel["next_idx"]]
        _add_submodels(root, parent, submodel, submodels)

    var child = submodels[created_submodel["first_child_idx"]] if created_submodel["first_child_idx"] > -1 else null
    #print("Child: ", child)
    if child:
        _add_submodels(root, created if created else parent, child, submodels)

func load_model(source_file, options={}) -> MaSzyna_Model:
    var file = File.new()
    #print("Importing: ", source_file)
    
    if file.open(source_file, File.READ) != OK:
        push_error("Cannot open source file: "+source_file)
        return null
        
    #print("Options: ", options)
    
    var node = MaSzyna_Model.new()
    var submodels = parse_e3d(file, options)
    node.name = source_file.get_file().get_basename()
    node.data_path = ""

    var parent = node
    banany = 0

    _add_submodels(node, node, submodels[0], submodels)

    if options.get("generate_collision"):
        var aabb = _extend_aabb_recursive(
            node,
            AABB(Vector3.ZERO, Vector3.ZERO)
        )
        #print("Collision AABB: ", aabb)
        var coll = CollisionShape.new()
        coll.shape = BoxShape.new()
        coll.transform.origin = Vector3(0, (aabb.size/2).y, 0)  # WTF?
        coll.shape.extents = aabb.size/2
        node.add_child(coll)
        coll.set_owner(node)

    return node
