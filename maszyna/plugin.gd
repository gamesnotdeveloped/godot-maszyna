tool
extends EditorPlugin

func add_custom_project_setting(name: String, default_value, type: int, hint: int = PROPERTY_HINT_NONE, hint_string: String = "") -> void:

    if ProjectSettings.has_setting(name): return

    var setting_info: Dictionary = {
        "name": name,
        "type": type,
        "hint": hint,
        "hint_string": hint_string
    }

    ProjectSettings.set_setting(name, default_value)
    ProjectSettings.add_property_info(setting_info)
    ProjectSettings.set_initial_value(name, default_value)

func _enter_tree():
    add_custom_type("MaSzyna_Triangles", "MeshInstance", preload("nodes/MaSzyna_Triangles.gd"), null)
    add_custom_type("MaSzyna_Include", "Spatial", preload("nodes/MaSzyna_Include.gd"), null)
    add_custom_type("MaSzyna_Model", "Spatial", preload("nodes/MaSzyna_Model.gd"), null)
    add_custom_type("MaSzyna_SubModel", "MeshInstance", preload("nodes/MaSzyna_SubModel.gd"), null)
    var inc_import_plugin = preload("inc_import_plugin.gd").new()
    var e3d_import_plugin = preload("e3d_import_plugin.gd").new()
    add_import_plugin(inc_import_plugin)
    add_import_plugin(e3d_import_plugin)
    
    add_custom_project_setting("maszyna/data_dir", "", TYPE_STRING, PROPERTY_HINT_GLOBAL_DIR)
    add_custom_project_setting("maszyna/import_model_scale_factor", 1.0, TYPE_REAL)
    
func _exit_tree():
    remove_custom_type("MaSzyna_Triangles")
    remove_custom_type("MaSzyna_Include")
    remove_custom_type("MaSzyna_Model")
    remove_custom_type("MaSzyna_SubModel")
