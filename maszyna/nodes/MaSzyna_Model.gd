tool
extends Spatial
class_name MaSzyna_Model

var _dynamic_materials = []
export(Array, String) var skins = [] setget set_skins

export var data_path:String = "" setget set_data_path

func is_maszyna_model():
    return true
    
func set_data_path(x):
    data_path = x
    #print("Setting data path ", x)
    _reload_skins()
    _propagate_dynamic_materials(self)

func set_skins(x):
    #print("Setting skins ", x)
    skins = x
    _reload_skins()
    _propagate_dynamic_materials(self)
    
var material_manager = MaSzynaMaterialManager.new()

func _propagate_dynamic_materials(node):
    var dynamic_materials_count = _dynamic_materials.size()
    
    for child in node.get_children():
        if child is MaSzyna_SubModel and child.dynamic_material:
            var mat_idx = child.dynamic_material_index
            if mat_idx < dynamic_materials_count:
                child.material_override = _dynamic_materials[mat_idx]
            else:
                child.material_override = null
        _propagate_dynamic_materials(child)

    
func _reload_skins():
    if skins.size():
        #print(self, " skins: ", skins)
        _dynamic_materials.clear()
        var mm = material_manager
        for skin in skins:
            #print(self, " loading skin ", skin)
            var m = SpatialMaterial.new()
            m.albedo_texture = mm.load_skin_texture(data_path, skin)
            _dynamic_materials.append(m)
    _propagate_dynamic_materials(self)
    
func _ready():
    _reload_skins()
