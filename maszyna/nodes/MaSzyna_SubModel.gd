tool
extends MeshInstance
class_name MaSzyna_SubModel


enum AnimationType {
    NONE=0,
    ROTATE_VEC=1,
    ROTATE_XYZ,
    MOVE,
    JUMP_SECONDS,
    JUMP_MINUTES,
    JUMP_HOURS,
    JUMP_HOURS24,
    SECONDS,
    MINUTES,
    HOURS,
    HOURS24,
    BILLBOARD,
    WIND,
    SKY,
    DIGITAL,
    DIGICLK,
    UNDEFINED,
    IK=256,
    IK1=257,
    IK2=258,
    UNKOWN=-1
}

export(AnimationType) var animation = AnimationType.NONE
export(float, 0.0, 1.0, 0.01) var light_threshold:float = 0.0
export(float, -1.0, 1.0, 0.01) var light_visibility:float = -1.0
export(Color, RGB) var self_illumination = Color(1.0, 1.0, 1.0)
export var dynamic_material:bool = false setget set_dynamic_material
export var dynamic_material_index:int = 0
export var material_name:String = "" setget set_material_name

var _dirty = false
var material_manager = MaSzynaMaterialManager.new()

func set_dynamic_material(x):
    dynamic_material = x
    _dirty = true

    
func set_material_name(x):
    material_name = x
    _dirty = true
    
    
func _find_model(node:Node):
    var parent = node.get_parent()
    if not parent:
        return null
    if parent.has_method("is_maszyna_model") and parent.is_maszyna_model():
        return parent
    return _find_model(parent)
    

func _reload_materials():
    if not dynamic_material and material_name:
        var model = _find_model(self)
        if model:
            var mm = material_manager
            #print("Model ",model)
            var tex = mm.load_submodel_texture(model.data_path, material_name)
            
            material_override = SpatialMaterial.new()
            material_override.albedo_texture = tex
            
func _ready():
    _reload_materials()
    
func _process(delta):
    if _dirty:
        _dirty = false
        _reload_materials()
