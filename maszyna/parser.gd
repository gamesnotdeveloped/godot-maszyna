class_name MaSzyna_Parser

var _file:File
var _handlers:Dictionary = {}


func _init(file: File):
    _file = file
    
func register_handler(token:String, instance: Object, method: String):
    _handlers[token] = funcref(instance, method)
    
func get_tokens(num: int, stop: Array = [" ", char(10), char(13), ';']):
    var tokens = []
    var c = ""
    var maybe_comment = false

    for t in range(num):
        var token = ""
        while not _file.eof_reached():
            c = char(_file.get_8())
            if c == "/":
                if maybe_comment:
                    maybe_comment = false
                    while not _file.eof_reached():
                        c = char(_file.get_8())
                        if c in [char(10), char(13)]:
                            break
                else:
                    maybe_comment = true
                    continue
            elif maybe_comment:
                maybe_comment = false
                        
            if c in stop:
                break 
            token += c
        token = token.strip_edges()
        if token:
            tokens.append(token)

    return tokens

func next_token(stop: Array = [" ", char(10), char(13), ';']) -> String:
    var tokens = get_tokens(1, stop)
    return tokens[0] if tokens else ""

func get_tokens_until(token: String, stop: Array = [" ", char(10), char(13), ';']):
    var tokens = []
    while not _file.eof_reached():
        var upcoming_token = next_token(stop)
        if upcoming_token:
            tokens.append(upcoming_token)
            if upcoming_token == token:
                break
    return tokens
    
func parse() -> Array:
    var result = []
    
    while not _file.eof_reached():
        var token = next_token()
        print("Token: "+token)
        if token.begins_with("//"):
            print(" skipped comment: ", next_token([char(10), char(13)]))
        elif token:
            if not token in _handlers:
                push_error("Unknown token: "+token)
                break
            else:
                var _parsed = _handlers[token].call_func(self)
                if _parsed:
                    result.append(_parsed)
    return result
