tool
extends EditorImportPlugin

enum Presets { DEFAULT }


func get_importer_name():
    return "maszyna.inc"
    
func get_visible_name():
    return "MaSzyna Include"

func get_recognized_extensions():
    return ["inc"]

func get_import_options(preset):
    return []
    
func get_preset_count():
    return Presets.size()
    
func get_preset_name(preset):
    match preset:
        Presets.DEFAULT:
            return "Default"
        _:
            return "Unknown"

func get_resource_type():
    return "PackedScene"
    
func get_save_extension():
    return "tscn"
    
func _parse_event(p: MaSzyna_Parser):
    p.get_tokens_until("endevent")
    return MaSzyna_Event.new()
    
    
func _parse_node(p: MaSzyna_Parser):
    var args = p.get_tokens(2)
    var name = p.next_token()
    var type = p.next_token()
    var obj
    
    print("node ", args, " "+name+" "+type)
    
    match type:
        "triangles":
            var material
            var texture
            
            var _n = p.next_token()
            match _n:
                "material":
                    material = p.get_tokens_until("endmaterial")
                    print("Node material: ", material)
                    texture = p.next_token()
                _:
                    texture = _n
            
            var data:Array = p.get_tokens_until("endtri")
            var mesh = ArrayMesh.new()
            var vertices = PoolVector3Array()
            var triangles = []
            var normals = PoolVector3Array()
            var uvs = PoolVector2Array()
            
            
            while data.size():
                var x = data.pop_front()
                var y = data.pop_front()
                var z = data.pop_front()
                var nx = data.pop_front()
                var ny = data.pop_front()
                var nz = data.pop_front()
                var u = data.pop_front()
                var v = data.pop_front()
                var _stop = data.pop_front()
                if not _stop in ["end", "endtri"]:
                    print(texture, " ", Vector3(x, y, z), " ", Vector3(nx, ny, nz), " ", Vector2(u, v), " ", _stop)
                    push_error("Incorrect triangle format")
                    return
                vertices.append(Vector3(float(x), float(y), float(z)))
                normals.append(Vector3(float(nx), float(ny), float(nz)))
                uvs.append(Vector2(float(u), float(v)))

            vertices.invert()
            normals.invert()
            uvs.invert()
            
            triangles.resize(ArrayMesh.ARRAY_MAX)
            triangles[ArrayMesh.ARRAY_VERTEX] = vertices
            triangles[ArrayMesh.ARRAY_NORMAL] = normals
            triangles[ArrayMesh.ARRAY_TEX_UV] = uvs

            mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, triangles)

            obj = MaSzyna_Triangles.new()
            obj.mesh = mesh
        "eventlauncher":
            p.get_tokens_until("end")
            obj = MaSzyna_EventLauncher.new()
        "model":
            p.get_tokens_until("endmodel")
            obj = MeshInstance.new()
        "sound":
            p.get_tokens_until("endsound")
            obj = AudioStreamPlayer3D.new()
        "memcell":
            p.get_tokens_until("endmemcell")
        _:
            push_error("Unknown node type: "+type)
            return null
    if obj:
        obj.name = name
    return obj

func _parse_origin(p: MaSzyna_Parser):
    var origin = p.get_tokens(3)
    print("Origin: ", origin)

func _parse_endorigin(p: MaSzyna_Parser):
    pass
    
func _parse_rotate(p: MaSzyna_Parser):
    var rotate = p.get_tokens(3)
    print("Rotate: ", rotate)
    
func import(source_file, save_path, options, r_platform_variants, r_gen_files):
    var file = File.new()
    var err = file.open(source_file, File.READ)
    if err != OK:
        return err

    var p = MaSzyna_Parser.new(file)
    p.register_handler("node", self, "_parse_node")
    p.register_handler("event", self, "_parse_event")
    p.register_handler("origin", self, "_parse_origin")
    p.register_handler("endorigin", self, "_parse_origin")
    p.register_handler("rotate", self, "_parse_rotate")
    
    var parsed_objects = p.parse()
    print("Parsed objects:")
    print(parsed_objects)

    file.close()
    
    var scene = PackedScene.new()
    var include = MaSzyna_Include.new()
    include.name = source_file.get_file().get_basename()

    for obj in parsed_objects:
        include.add_child(obj)
        obj.owner = include
    
    var result = scene.pack(include)
    if result == OK:
        return ResourceSaver.save("%s.%s" % [save_path, get_save_extension()], scene)
