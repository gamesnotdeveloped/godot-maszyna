tool
extends EditorImportPlugin



func get_recognized_extensions():
    return ["e3d"]

func get_resource_type():
    return "PackedScene"

func get_importer_name():
    return "maszyna.e3d"

func get_import_order() -> int:
    return 10

func get_import_options(preset):
    return [{"name": "generate_collision", "default_value": true}]

func get_visible_name():
    return "MaSzyna E3D Importer"

func get_save_extension():
    return "tscn"

func get_preset_count():
    return 0

func get_priority():
    return 1

func get_preset_name(i):
    return "default"


func _get_option_visibility(path: String, option_name: String, options: Dictionary) -> bool:
    return option_name != "mode"




func import(source_file, save_path, options, platform_variants, gen_files):
    var file = File.new()
    #print("Importing: ", source_file)
    if file.open(source_file, File.READ) != OK:
        return FAILED

    var scene = PackedScene.new()
    var parser = E3DParser.new()
    var node = parser.load_model(source_file, options)
    scene.pack(node)

    var filename = save_path + "." + get_save_extension()
    #print("E3D saving as ", filename)
    return ResourceSaver.save(filename, scene)
