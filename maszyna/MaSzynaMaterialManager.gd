extends Object
class_name MaSzynaMaterialManager

var _cache = {}
var matparser = MaSzynaMatParser.new()

func _init(cache:Dictionary={}):
    if cache:
        _cache = cache

func load_material(model_path, material_name) -> MaSzyna_Material:
    return matparser.parse(model_path+"/"+material_name+".mat")

func load_texture(model_path, material_name, global:bool) -> ImageTexture:
    var im = Image.new()
    var project_data_dir = ProjectSettings.get_setting("maszyna/data_dir")
    var data_dir = project_data_dir if project_data_dir else ""
    var texture_path = model_path
    if global:
        texture_path = "textures"
    var final_path = project_data_dir+"/"+texture_path+"/"+material_name+".dds"
    #print("Loading texture: ", final_path)
    var err = im.load(final_path)
    var it = ImageTexture.new()
    #var err = it.load(final_path)
    if err == OK:
        #print("Loaded texture: %s %sx%s" % [im.get_format(), im.get_width(), im.get_height()])
        it.create_from_image(im)
    else:
        push_error("Error loading texture: " + String(err))
    return it
        

func load_submodel_texture(model_path, material_name) -> ImageTexture:
    return load_texture(model_path, material_name, "/" in material_name)
    
func load_skin_texture(model_path, material_name) -> ImageTexture:
    return load_texture(model_path, material_name, false)
